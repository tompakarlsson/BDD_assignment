Feature: Day one part one
    Calculate fuel. Fuel required to launch a given module is based on its mass.
    Specifically, to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.

    Scenario Outline: Outlined given, when, then
        Given I have a <mass>

        When It is divided by three, rounded down and subtracted by 2

        Then The needed fuel should be <result>

        Examples:
        | mass   | result |
        | 12     | 2      |
        | 14     | 2      |
        | 1969   | 654    |
        | 100756 | 33583  |
