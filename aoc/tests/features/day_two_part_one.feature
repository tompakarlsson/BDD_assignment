Feature: Day two part one
    Iterate over a list of integers - intcode - which is interpreted in chunks of four.
    The first integer tell us if we should add (1), multiply (2) or halt (99).
    The second and third integer tell us which positions to perform addition or multipliaton with.
    The fourth integer tell us where to put our answer.

    Scenario Outline: Outlined given, when, then
        Given I have a <program> in intcode

        When I perform the needed calculations

        Then I get a <value> at position 0

        Examples:
        | program                          | value |
        | 1,9,10,3,2,3,11,0,99,30,40,50,68 | 3500  |
