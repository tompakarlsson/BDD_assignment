Feature: Day one part two
    Fuel itself requires fuel just like a module - take its mass, divide by three, round down, and subtract 2.
    However, that fuel also requires fuel, and that fuel requires fuel, and so on.
    Any mass that would require negative fuel should instead be treated as if it requires zero fuel; the remaining mass,
    if any, is instead handled by wishing really hard, which has no mass and is outside the scope of this calculation.

    Scenario Outline: Outlined given, when, then
        Given I have a <mass>

        When Its fuel requirements are calculated recursively until it no longer needs fuel

        Then Calculate the <total> amount of fuel needed for a module and its fuel

        Examples:
        | mass   | total |
        | 14     | 2     |
        | 1969   | 966   |
        | 100756 | 50346 |