Feature: Day two part two
    Using the intcode computer from part one, find the pair of of inputs that produces the output 19690720.
    In this program, the value placed in address 1 is called the noun, and the value placed in address 2 is called
    the verb. Each of the two input values will be between 0 and 99, inclusive.
    Return 100 * noun + verb.

    Scenario Outline: Outlined given, when, then
        Given I have a <program> in intcode

        When I call the function with a desired end <value>

        Then I will get a <result>

        Examples:
        | program                           | value | result |
        | 1,9,10,3,2,3,11,0,99,30,40,50,68  | 3450  | 12     |
