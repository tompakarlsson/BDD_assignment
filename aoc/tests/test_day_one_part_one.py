"""Day one part one feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)

from aoc.src.day_one_part_one import calculate_fuel


@scenario('features/day_one.feature', 'Outlined given, when, then')
def test_outlined_given_when_then():
    """Outlined given, when, then."""


@given(parsers.parse('I have a {mass:d}'), target_fixture='challenge_input')
def i_have_a_mass(mass):
    """I have a <mass>."""
    return mass


@when('It is divided by three, rounded down and subtracted by 2', target_fixture='fuel_result')
def it_is_divided_by_three_rounded_down_and_subtracted_by_2(challenge_input):
    """It is divided by three, rounded down and subtracted by 2."""
    challenge_input = [challenge_input]
    return calculate_fuel(challenge_input)


@then(parsers.parse('The needed fuel should be {result:d}'))
def the_needed_fuel_should_be_result(fuel_result, result):
    """The needed fuel should be <result>."""
    assert fuel_result == result

