"""Day two part two feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)

from aoc.src.day_two_part_two import find_pair


@scenario('features/day_two_part_two.feature', 'Outlined given, when, then')
def test_outlined_given_when_then():
    """Outlined given, when, then."""


@given(parsers.parse('I have a {program} in intcode'), target_fixture='challenge_input')
def i_have_a_program_in_intcode(program):
    """I have a <program> in intcode."""
    return program


@when(parsers.parse('I call the function with a desired end {value:d}'), target_fixture='puzzle_output')
def i_call_the_function_with_a_desired_end_value(challenge_input, value):
    """I call the function with a desired end <value>."""
    challenge_input = list(map(int, challenge_input.split(',')))
    return find_pair(challenge_input, value)


@then(parsers.parse('I will get a {result:d}'))
def i_will_get_a_result(puzzle_output, result):
    """I will get a <result>."""
    assert puzzle_output == result

