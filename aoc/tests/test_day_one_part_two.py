"""Day one part two feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)

from aoc.src.day_one_part_two import calculate_recursive_fuel


@scenario('features/day_one_part_two.feature', 'Outlined given, when, then')
def test_outlined_given_when_then():
    """Outlined given, when, then."""


@given(parsers.parse('I have a {mass:d}'), target_fixture='challenge_input')
def i_have_a_mass(mass):
    """I have a <mass>."""
    return mass


@when('Its fuel requirements are calculated recursively until it no longer needs fuel', target_fixture='fuel_result')
def its_fuel_requirements_are_calculated_recursively_until_it_no_longer_needs_fuel(challenge_input):
    """Its fuel requirements are calculated recursively until it no longer needs fuel."""
    challenge_input = [challenge_input]
    return calculate_recursive_fuel(challenge_input)


@then(parsers.parse('Calculate the {total:d} amount of fuel needed for a module and its fuel'))
def calculate_the_total_amount_of_fuel_needed_for_a_module_and_its_fuel(fuel_result, total):
    """Calculate the <total> amount of fuel needed for a module and its fuel."""
    assert fuel_result == total

