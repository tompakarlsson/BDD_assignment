"""Day two part one feature tests."""

from pytest_bdd import (
    given,
    scenario,
    then,
    when,
    parsers
)

from aoc.src.day_two_part_one import interpret_intcode


@scenario('features/day_two_part_one.feature', 'Outlined given, when, then')
def test_outlined_given_when_then():
    """Outlined given, when, then."""


@given(parsers.parse('I have a {program} in intcode'), target_fixture='challenge_input')
def i_have_a_program_in_intcode(program):
    """I have a <program> in intcode."""
    return program


@when('I perform the needed calculations', target_fixture='puzzle_output')
def i_perform_the_needed_calculations(challenge_input):
    """I perform the needed calculations."""
    challenge_input = list(map(int, challenge_input.split(',')))
    return interpret_intcode(challenge_input)


@then(parsers.parse('I get a {value:d} at position 0'))
def i_get_a_value_at_position_0(puzzle_output, value):
    """I get a <value> at position 0."""
    assert puzzle_output == value

