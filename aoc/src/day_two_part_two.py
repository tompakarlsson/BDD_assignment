"""Advent of code 2019, day 2, part 2"""


def find_pair(input_data, end_result):
    for noun in range(100):
        for verb in range(100):
            asd = input_data.copy()
            asd[1] = noun
            asd[2] = verb
            for i in range(0, len(asd), 4):
                operator = asd[i]
                first = asd[i + 1]
                second = asd[i + 2]
                position = asd[i + 3]
                match operator:
                    case 1:
                        asd[position] = asd[first] + asd[second]
                    case 2:
                        asd[position] = asd[first] * asd[second]
                    case 99:
                        break
            if asd[0] == end_result:
                return 100 * asd[1] + asd[2]


if __name__ == '__main__':  # pragma: no cover
    with open('../input_data/input_day_two.txt', 'r') as file:
        data = file.read().split(',')
        data = list(map(int, data))
    print(find_pair(data, 19690720))
