"""Advent of code 2019, day 2, part 1"""


def interpret_intcode(input_data):
    input_data[1] = 12
    input_data[2] = 2
    for i in range(0, len(input_data)-1, 4):
        operator = input_data[i]
        first = input_data[i+1]
        second = input_data[i+2]
        position = input_data[i+3]
        match operator:
            case 1:
                input_data[position] = input_data[first] + input_data[second]
            case 2:
                input_data[position] = input_data[first] * input_data[second]
            case 99:
                break
    return input_data[0]


if __name__ == '__main__':  # pragma: no cover
    with open('../input_data/input_day_two.txt', 'r') as file:
        data = file.read().split(',')
        data = list(map(int, data))
    print(interpret_intcode(data))
