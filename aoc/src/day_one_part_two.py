"""Advent of code 2019, day 1, part 2"""


def calculate_recursive_fuel(input_data):
    total_fuel = 0
    for mass in input_data:
        fuel = mass // 3 - 2
        recursive_fuel = fuel

        while fuel >= 6:
            fuel = fuel // 3 - 2
            recursive_fuel += fuel

        total_fuel += recursive_fuel
    return total_fuel


if __name__ == '__main__':  # pragma: no cover
    with open('../input_data/input_day_one.txt', 'r') as file:
        data = [int(x) for x in file.read().splitlines()]
    print(calculate_recursive_fuel(data))
