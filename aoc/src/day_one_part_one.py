"""Advent of code 2019, day 1, part 1"""


def calculate_fuel(input_data: int | list[int]) -> int:
    return sum(mass // 3 - 2 for mass in input_data)


if __name__ == '__main__':  # pragma: no cover
    with open('../input_data/input_day_one.txt', 'r') as file:
        data = [int(x) for x in file.read().splitlines()]
    print(calculate_fuel(data))
