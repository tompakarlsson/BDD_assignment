[![pipeline status](https://gitlab.com/tompakarlsson/tdd_assignment/badges/main/pipeline.svg)](https://gitlab.com/tompakarlsson/tdd_assignment/-/commits/main)  [![coverage report](https://gitlab.com/tompakarlsson/tdd_assignment/badges/main/coverage.svg)](https://gitlab.com/tompakarlsson/tdd-assignment/-/commits/main)  
# Behaviour Driven Development  
De första två dagarna av Advent of Code 2019 gjorda genom BDD.  

Min spontana tanke efter jag provat att arbeta med BDD är att det är pissdrygt! Men det har nog att göra med att det är svårt att se fördelarna med BDD när man jobbar solo. I ett tvärfunktionellt team kan det ju såklart finnas en nytta av att ha ett gemensamt "språk" att utgå ifrån.  
Jag får också intrycket av att BDD ska användas i någon sorts agil process för att komma till sin rätt. Även det är ju svårt att efterlikna solo.
  
Om jag ska jämföra TDD med BDD så föredrar jag att jobba med simpel TDD. Färdsträckan är kortare och resultatet blir (för mig) detsamma. BDD är ju, som jag förstår det, en vidareutveckling av TDD på sätt och vis. Och då fördelarna med BDD är svåra att nå solo så återstår egentligen bara TDD.  

Sedan tidigare har jag börjat strukturera mina tester enligt given/when/then för att få bättre struktur över dem. Det är ju inte närmelsevis BDD, men jag gillar det sättet att formulera kod/tester.

